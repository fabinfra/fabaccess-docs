# Measurements
Durch "Measurements" in FabAccess werden Daten von Ressourcen gesammelt, um deren Leistung und Nutzung zu erfassen. Das Ziel ist es, dass FabAccess diese Messwerte sammelt und an die Nutzer weitergibt.

Die Funktion Measurements ermöglicht es FabAccess, wichtige Daten über die Nutzung von Ressourcen zu erfassen und den Nutzern zur Verfügung zu stellen. Dies umfasst Informationen wie Betriebsstunden, Auslastung und andere Leistungsindikatoren, die es den Nutzern ermöglichen, die Effizienz und Produktivität ihrer Arbeitsprozesse zu optimieren.

**Das Konzept zu Projekten exitiert zwar, jedoch gibt es noch keine Spezifikation!**