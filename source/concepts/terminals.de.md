# Terminals
Terminals ermöglichen einen eingeschränkten Zugriff auf den Server in FabAccess. Diese Terminals können nur auf ihnen zugewiesene Maschinen zugreifen und haben die Fähigkeit, Maschinen für andere Nutzer auszuleihen.

Aufgrund ihrer beschränkten Zugriffsrechte eignen sich Terminals ideal für die Authentifizierung in FabFire. Durch die Verwendung von Terminals als Authentifizierungsmethode können Nutzer sicherstellen, dass nur autorisierte Benutzer auf die Ressourcen zugreifen und diese nutzen können.

Die Verwendung von Terminals für die Authentifizierung bietet eine zusätzliche Sicherheitsebene und trägt dazu bei, die Integrität des Systems zu gewährleisten. Darüber hinaus ermöglicht es eine effiziente Verwaltung der Ressourcennutzung, indem nur autorisierten Benutzern Zugriff gewährt wird.