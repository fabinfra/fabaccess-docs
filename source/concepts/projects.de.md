# Projects
Projekte in FabAccess sollen die Zusammenarbeit zwischen den Nutzern fördern und gleichzeitig die Abrechnung von Maschinenzeiten verbessern. Ein wichtiger Aspekt der Projektstruktur ist die Möglichkeit für Nutzer, Claims innerhalb desselben Projekts miteinander zu teilen, um gleichzeitig auf Ressourcen zugreifen zu können.

Durch die Zuweisung von Nutzern zu Projekten können Teams effizient zusammenarbeiten und ihre Ressourcen optimal nutzen. Das Teilen von Claims innerhalb eines Projekts ermöglicht es den Teammitgliedern, nahtlos auf benötigte Ressourcen zuzugreifen und gemeinsam an Projekten zu arbeiten. Darüber hinaus erleichtert diese Funktionalität die Abrechnung von Maschinenzeiten, da die Nutzung der Ressourcen innerhalb eines Projekts besser nachverfolgt und zugeordnet werden kann.

**Das Konzept zu Projekten exitiert zwar, jedoch gibt es noch keine Spezifikation!**