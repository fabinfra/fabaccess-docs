# Claim Konzept
Das Claim Konzept dienen als Abstraktion des Verleihens einer Ressource in FabAccess. Ihr Hauptzweck besteht darin, die Möglichkeiten zu verwalten, wie Benutzer Zugriff auf eine Ressource erhalten und diesen Zugriff untereinander teilen können.

## Claims
Ein "Claim" in FabAccess repräsentiert den gewährten Zugriff auf eine Ressource.

Die Flexibilität von Claims ermöglicht es, verschiedene Szenarien des Ressourcenzugriffs effektiv zu unterstützen. Zum Beispiel können mehrere Benutzer gleichzeitig einen Claim für eine Ressource erhalten, was besonders in Umgebungen mit kollaborativem Arbeiten von Vorteil ist. Darüber hinaus bietet die Möglichkeit, sich in eine Warteschlange mit einem Interest einzutragen, eine praktische Lösung für Situationen, in denen die Verfügbarkeit einer Ressource begrenzt ist und Benutzer darauf warten müssen, darauf zugreifen zu können.

Ein weiterer wichtiger Aspekt von Claims ist ihre Fähigkeit, Ressourcen zwischen Benutzern zu transferieren oder auszuleihen. Diese Funktionen ermöglichen eine flexible Nutzung der Ressourcen und fördern die Zusammenarbeit zwischen den Benutzern. Zum Beispiel kann ein Benutzer, der eine Ressource nicht mehr benötigt, diese einem anderen Benutzer übertragen, der sie gerade benötigt, oder ein Ausbilder kann einem Auszubildenden vorübergehend Zugriff auf eine Ressource gewähren, um bestimmte Fähigkeiten zu erlernen.

## Interest
Ein wichtiger Bestandteil des Claims-Konzepts sind "Interest", die Reservierungen abbilden, die entweder zeitbasiert oder auf einer Warteschlange basieren können. Diese Interessen bieten den Benutzern die Möglichkeit, einen zukünftigen Zugriff auf eine Ressource zu sichern, entweder basierend auf einer vordefinierten Zeit oder in der Reihenfolge des Eintritts in die Warteschlange.

Mit einem Interest signalisiert der Nutzer dem System sein Interesse an einer bestimmten Ressource. Bei der nächsten Gelegenheit erhält der Nutzer entweder einen Claim auf die Ressource oder hat die Möglichkeit, seinen Interest auf einen Claim zu aktualisieren. Diese Flexibilität ermöglicht es den Benutzern, ihre Bedürfnisse anzupassen und auf Änderungen in der Verfügbarkeit der Ressourcen zu reagieren.


## Notify
Das letzte Element des Claim-Konzepts ist der "Notify". Durch den Notify können Nutzer den Status einer Ressource einsehen und sich über Änderungen benachrichtigen lassen.

Der Notify ermöglicht es Benutzern, den aktuellen Zustand einer Ressource abzurufen und bei Bedarf auf Änderungen zu reagieren. Dies bietet eine wichtige Möglichkeit, den Zustand von Ressourcen zu überwachen und zeitnah auf relevante Ereignisse zu reagieren.

Durch die Möglichkeit, sich für Benachrichtigungen über Zustandsänderungen zu registrieren, können Benutzer effektiv mit den Ressourcen interagieren und sicherstellen, dass sie stets über wichtige Entwicklungen informiert sind.