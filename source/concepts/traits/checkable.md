# Checkable
Der komplexere Trait "Checkable" ermöglicht die Abbildung von Ressourcen, die nach der Benutzung überprüft werden müssen. Bei einer Überprüfung durch einen berechtigten Nutzer kann die Ressource entweder für alle wieder freigegeben oder zurückgewiesen werden. Falls die Ressource zurückgewiesen wurde, kann der ursprüngliche Nutzer die Ressource weiterhin verwenden oder erneut zur Überprüfung einreichen, nachdem die Fehler behoben wurden.

## OID
`1.3.6.1.4.1.61783.612.1.3`

## States
```mermaid
stateDiagram
    [*] --> FREE
    FREE --> INUSE: use
    INUSE --> CHECK: giveback
    CHECK --> FREE: accept
    CHECK --> REJECTED: reject
    REJECTED --> INUSE: use
    REJECTED --> CHECK: giveback
    REJECTED --> FREE: accept
```