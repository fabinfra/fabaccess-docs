# Powerable
"Powerable" ist der grundlegendste Trait, den FabAccess unterstützt. Er dient dazu abzubilden, ob eine Ressource eingeschaltet bzw. mit Strom versorgt ist.

## OID
`1.3.6.1.4.1.61783.612.1.1`

## States
```mermaid
stateDiagram
    [*] --> OFF
    OFF --> ON: turnON
    ON --> OFF: turnOFF
```